import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.sqlite.SQLiteDataSource;

public class InsertRowExample {
	public static void main(String[] args) {
		SQLiteDataSource ds = null;

		try {
			ds = new SQLiteDataSource();
			ds.setUrl("jdbc:sqlite:test.db");
		} catch ( Exception e ) {
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println( "Opened database successfully" );
		
		System.out.println( "Attempting to insert two rows into test table" );
		String query1 = "INSERT INTO test ( NAME ) VALUES ( 'Shane' )";
		String query2 = "INSERT INTO test ( NAME ) VALUES ( 'Sharon' )";
		try ( Connection conn = ds.getConnection();
				Statement stmt = conn.createStatement(); ) {
			int rv = stmt.executeUpdate( query1 );
			System.out.println( "1st executeUpdate() returned " + rv );
			
			rv = stmt.executeUpdate( query2 );
			System.out.println( "2nd executeUpdate() returned " + rv );
		} catch ( SQLException e ) {
			e.printStackTrace();
			System.exit( 0 );
		}
	}
}
