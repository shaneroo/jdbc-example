import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.sqlite.SQLiteDataSource;

public class SelectRowsExample {
	public static void main(String[] args) {
		SQLiteDataSource ds = null;

		try {
			ds = new SQLiteDataSource();
			ds.setUrl("jdbc:sqlite:test.db");
		} catch ( Exception e ) {
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println( "Opened database successfully" );
		
		System.out.println( "Selecting all rows from test table" );
		String query = "SELECT * FROM test";
		try ( Connection conn = ds.getConnection();
				Statement stmt = conn.createStatement(); ) {
			ResultSet rs = stmt.executeQuery(query);
			
			while ( rs.next() ) {
				int id = rs.getInt( "ID" );
				String name = rs.getString( "NAME" );
				
				System.out.println( "Result: ID = " + id + ", NAME = " + name );
			}
		} catch ( SQLException e ) {
			e.printStackTrace();
			System.exit( 0 );
		}
	}
}
