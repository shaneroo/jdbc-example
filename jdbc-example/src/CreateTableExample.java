import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.sqlite.SQLiteDataSource;

public class CreateTableExample {

	public static void main(String[] args) {
		SQLiteDataSource ds = null;

		try {
			ds = new SQLiteDataSource();
			ds.setUrl("jdbc:sqlite:test.db");
		} catch ( Exception e ) {
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println( "Opened database successfully" );
		
		String query = "CREATE TABLE IF NOT EXISTS test ( " +
				"ID INTEGER PRIMARY KEY, " +
				"NAME TEXT NOT NULL )";
		try ( Connection conn = ds.getConnection();
				Statement stmt = conn.createStatement(); ) {
			int rv = stmt.executeUpdate( query );
			System.out.println( "executeUpdate() returned " + rv );
		} catch ( SQLException e ) {
			e.printStackTrace();
			System.exit( 0 );
		}
		System.out.println( "Created database successfully" );
	}
}