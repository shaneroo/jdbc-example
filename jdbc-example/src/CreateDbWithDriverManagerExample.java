import java.sql.Connection;
import java.sql.DriverManager;

public class CreateDbWithDriverManagerExample {

	public static void main(String[] args) {
		try (Connection conn = DriverManager.getConnection("jdbc:sqlite:test.db");) {
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println("Opened database successfully");
	}

}