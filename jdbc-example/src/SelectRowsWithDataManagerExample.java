import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectRowsWithDataManagerExample {
	public static void main(String[] args) throws SQLException {
		Connection c = null;
		ResultSet rs = null;

		try {
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Opened database successfully");

		System.out.println("Selecting all rows from test table");
		Statement stmt = null;
		String query = "SELECT * FROM test";
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery(query);
			
			while ( rs.next() ) {
				int id = rs.getInt( "ID" );
				String name = rs.getString( "NAME" );
				
				System.out.println( "Result: ID = " + id + ", NAME = " + name );
			}
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

}
