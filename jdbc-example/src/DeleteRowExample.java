import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.sqlite.SQLiteDataSource;

public class DeleteRowExample {
	public static void main(String[] args) {
		SQLiteDataSource ds = null;

		try {
			ds = new SQLiteDataSource();
			ds.setUrl("jdbc:sqlite:test.db");
		} catch ( Exception e ) {
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println( "Opened database successfully" );
		
		System.out.println( "Attempting to delete a row from test table" );
		String query = "DELETE FROM test WHERE NAME = 'Shane'";
		try ( Connection conn = ds.getConnection();
				Statement stmt = conn.createStatement(); ) {
			int rv = stmt.executeUpdate( query );
			System.out.println( "executeUpdate() returned " + rv );
		} catch ( SQLException e ) {
			e.printStackTrace();
			System.exit( 0 );
		}
	}
}
